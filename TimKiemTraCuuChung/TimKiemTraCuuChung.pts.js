import PropTypes from 'prop-types'

export const propTypes = {
  className: PropTypes.string,
  chiTietThuaDat: PropTypes.object,
  xuLyTimKiemTraCuu: PropTypes.func,
  xuLyTimKiemTraCuuChiTietThuaDat: PropTypes.func,
}

export const defaultProps = {
  className: '',
  chiTietThuaDat: {},
  xuLyTimKiemTraCuu: () => {},
  xuLyTimKiemTraCuuChiTietThuaDat: () => {},
}
