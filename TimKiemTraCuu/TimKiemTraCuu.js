import React, { useEffect, useState } from 'react'
import { useForm, useFieldArray, Controller } from 'react-hook-form'
import * as R from 'ramda'
import Constants from '@constants'
import { truongLoi, truyCapGiaTriHienTai, ktLoaiTSTheoLoaiTimKiem, ktRong } from '@utils/common'
import { Tab, ChonKhuVuc, DangXuLy, SelectWrapper } from '@components'
import { DSGoiYTimKiemTraCuu } from '@ban-do-citics'

import { propTypes, defaultProps } from './TimKiemTraCuu.pts'

const nhomTimeout = {}

const TimKiemTraCuu = (props) => {
  const { chiTimCanHo, chiTimNhaDat, loc = {}, chiTietThuaDat, maTimKiemTraCuu } = props

  useEffect(() => {
    chiTimCanHo && capNhatLoaiTimKiem(Constants.LOAI_TAI_SAN.CAN_HO)
    chiTimNhaDat && capNhatLoaiTimKiem(Constants.LOAI_TAI_SAN.NHA_DAT)
  }, [chiTimCanHo, chiTimNhaDat])

  const diaChiThuaDatDangChon = R.pathOr('', ['duLieu', 'properties', 'dia_chi'])(chiTietThuaDat)
  const [loaiTimKiem, capNhatLoaiTimKiem] = useState(loc.loaiTimKiem || Constants.LOAI_TIM_KIEM.NHA_DAT)

  const ktLoaiTS = ktLoaiTSTheoLoaiTimKiem(loaiTimKiem)

  const loaiTimKiemNhaDat = R.pathOr(false, ['traCuuNhaDat'])(ktLoaiTS)
  const loaiTimKiemDuAn = R.pathOr(false, ['traCuuDuAn'])(ktLoaiTS)

  const [hienTimKiemNhaDat, anHienTimKiemNhaDat] = useState(loaiTimKiemNhaDat)
  const [hienTimKiemDuAn, anHienTimKiemDuAn] = useState(loaiTimKiemDuAn)
  const [tabTimKiemNhaDat, capNhatTabTimKiemNhaDat] = useState(
    loaiTimKiem === Constants.LOAI_TIM_KIEM.VN_2000 || loaiTimKiem === Constants.LOAI_TIM_KIEM.WGS_84
      ? 'tabToaDo'
      : loaiTimKiem === Constants.LOAI_TIM_KIEM.SO_TO_SO_THUA
      ? 'tabToSoThuaSo'
      : 'tabDiaChi'
  )

  const thuocTinhTabTimKiemTraCuu = {
    xuLyTab: (data) => {
      capNhatLoaiTimKiem(data.ma)
      anHienTimKiemNhaDat(data.ma === Constants.LOAI_TIM_KIEM.NHA_DAT)
      anHienTimKiemDuAn(data.ma === Constants.LOAI_TIM_KIEM.DS_DU_AN)
    },
    dsTab: [
      ...(!chiTimCanHo
        ? [
            {
              ma: Constants.LOAI_TIM_KIEM.NHA_DAT,
              dsMa: [
                Constants.LOAI_TIM_KIEM.GOOGLE,
                Constants.LOAI_TIM_KIEM.WGS_84,
                Constants.LOAI_TIM_KIEM.VN_2000,
                Constants.LOAI_TIM_KIEM.NHA_DAT,
                Constants.LOAI_TIM_KIEM.SO_TO_SO_THUA,
              ],
              ten: (
                <>
                  <i className="icon icon-ngoi-nha-vien" />
                </>
              ),
            },
          ]
        : []),
      ...(!chiTimNhaDat
        ? [
            {
              ma: Constants.LOAI_TIM_KIEM.DS_DU_AN,
              dsMa: [Constants.LOAI_TIM_KIEM.DS_DU_AN, Constants.LOAI_TIM_KIEM.DU_AN],
              ten: (
                <>
                  <i className="icon icon-toa-nha-don-vien" />
                </>
              ),
            },
          ]
        : []),
    ],
    tabDangChon: {
      ma: loaiTimKiem,
    },
    loaiTab: 2,
  }

  const thuocTinhTabTimKiemNhaDat = {
    className: 'c-tab-chia-deu',
    xuLyTab: (tab) => {
      capNhatTabTimKiemNhaDat(tab.ma)
    },
    dsTab: [
      {
        ma: 'tabDiaChi',
        ten: 'Theo địa chỉ',
      },
      {
        ma: 'tabToSoThuaSo',
        ten: 'Theo tờ số, thửa số',
      },
      {
        ma: 'tabToaDo',
        ten: 'Theo tọa độ',
      },
    ],
    tabDangChon: {
      ma: tabTimKiemNhaDat,
    },
    loaiTab: 1,
  }

  const dongTimKiemNhaDat = () => {
    anHienTimKiemNhaDat(false)
  }

  const dongTimKiemDuAn = () => {
    anHienTimKiemDuAn(false)
  }

  useEffect(() => {
    document.addEventListener('click', dongTimKiemNhaDat)
    document.addEventListener('click', dongTimKiemDuAn)

    return () => {
      document.removeEventListener('click', dongTimKiemNhaDat)
      document.removeEventListener('click', dongTimKiemDuAn)
    }
  }, [])

  useEffect(() => {
    dongTimKiemNhaDat()
    dongTimKiemDuAn()
  }, [chiTietThuaDat.duLieu])

  useEffect(() => {
    const loaiTimKiem = loc.loaiTimKiem || Constants.LOAI_TIM_KIEM.NHA_DAT

    capNhatLoaiTimKiem(loaiTimKiem)
  }, [loc])

  const thuocTinhTimKiemDiaChi = {
    ...props,
    dongTimKiemNhaDat,
    loaiTimKiem: Constants.LOAI_TIM_KIEM.NHA_DAT,
  }
  const thuocTinhTimKiemDuAn = {
    ...props,
    dongTimKiemDuAn,
    className: 'c-tim-kiem-tra-cuu-du-an',
    loaiTimKiem: Constants.LOAI_TIM_KIEM.DS_DU_AN,
  }
  const thuocTinhTimKiemToSoThuaSo = {
    ...props,
  }
  const thuocTinhTimKiemToaDo = {
    ...props,
  }

  return (
    <div
      id={maTimKiemTraCuu || 'timKiemTraCuu'}
      className={`c-tim-kiem-tra-cuu ${hienTimKiemNhaDat ? 'dang-mo' : 'dang-dong'}`}
      onClick={(e) => {
        e.stopPropagation()
      }}
    >
      <div className="c-tim-kiem-tra-cuu-loai">
        <Tab {...thuocTinhTabTimKiemTraCuu} />
        {!chiTimNhaDat && hienTimKiemDuAn && loaiTimKiemDuAn && <TimKiemDiaChi {...thuocTinhTimKiemDuAn} />}
        {!hienTimKiemNhaDat && !hienTimKiemDuAn && (
          <input
            type="text"
            value={diaChiThuaDatDangChon || loc.tuKhoa || ''}
            readOnly={true}
            placeholder={
              hienTimKiemDuAn
                ? 'Nhập tên dự án hoặc khu vực bạn muốn tìm kiếm ...'
                : 'Nhập địa chỉ nhà bạn muốn tìm kiếm...'
            }
            onClick={() => {
              anHienTimKiemNhaDat(loaiTimKiemNhaDat)
              anHienTimKiemDuAn(loaiTimKiemDuAn)
            }}
          />
        )}
      </div>
      {!chiTimCanHo && hienTimKiemNhaDat && loaiTimKiemNhaDat && (
        <div className="c-tim-kiem-tra-cuu-nha-dat">
          <Tab {...thuocTinhTabTimKiemNhaDat} />
          {tabTimKiemNhaDat === 'tabDiaChi' ? (
            <TimKiemDiaChi {...thuocTinhTimKiemDiaChi} />
          ) : tabTimKiemNhaDat === 'tabToSoThuaSo' ? (
            <TimKiemToSoThuaSo {...thuocTinhTimKiemToSoThuaSo} />
          ) : (
            <TimKiemToaDo {...thuocTinhTimKiemToaDo} />
          )}
        </div>
      )}
    </div>
  )
}

TimKiemTraCuu.propTypes = propTypes
TimKiemTraCuu.defaultProps = defaultProps

export const TimKiemDiaChi = (props) => {
  // Tìm nhà đất & dự án theo từ khoá
  const {
    loc,
    truCP,
    prefixLoc,
    prefixBanDo,
    khongSoSanh,
    loaiTimKiem,
    className = '',
    dongTimKiemDuAn,
    dongTimKiemNhaDat,
    xuLyTimKiemTraCuu,
    dsGoiYTimKiemTraCuu,
    xuLyTimKiemTraCuuDuAn,
    xuLyTimKiemTraCuuDSDuAn,
    xuLyTimKiemTraCuuChiTietThuaDat,
  } = props
  const { register, setFocus, reset } = useForm({
    defaultValues: loc,
  })
  const [hienGoiY, anHienGoiY] = useState(false)

  useEffect(() => {
    setFocus('tuKhoa', { shouldSelect: true })
  }, [setFocus])

  useEffect(() => {
    nhomTimeout.traCuuNhaDat = null

    xuLyTimKiemTraCuu(prefixLoc)({
      loaiTimKiem,
      tuKhoa: R.pathOr('', ['tuKhoa'])(loc),
    })

    return () => {
      xuLyAnHienGoiYTimKiemTraCuu(false)
    }
  }, [])

  const xuLyAnHienGoiYTimKiemTraCuu = (trangThai) => {
    anHienGoiY(trangThai)
  }

  const thuocTinhDSGoiYTimKiemTraCuu = {
    loc,
    truCP,
    khongSoSanh,
    dongTimKiemDuAn,
    dongTimKiemNhaDat,
    dsGoiYTimKiemTraCuu,
    xuLyTimKiemTraCuuDuAn,
    xuLyTimKiemTraCuuDSDuAn,
    xuLyTimKiemTraCuuChiTietThuaDat,
    prefixBanDo,
    prefixLoc,
  }

  return (
    <form
      className={className}
      onSubmit={(e) => {
        e.preventDefault()
      }}
    >
      <div className="truong truong-co-tac-vu truong-tim-kiem">
        <input
          type="text"
          autoComplete="off"
          {...register('tuKhoa', {
            onChange: (e) => {
              const tuKhoa = R.pathOr('', ['currentTarget', 'value'])(e)

              nhomTimeout.traCuuNhaDat && clearTimeout(nhomTimeout.traCuuNhaDat)
              nhomTimeout.traCuuNhaDat = setTimeout(() => {
                xuLyTimKiemTraCuu(prefixLoc)({
                  loaiTimKiem,
                  tuKhoa,
                })
              }, 500)
            },
          })}
          placeholder="Nhập địa chỉ nhà bạn muốn tìm kiếm..."
          onFocus={() => {
            xuLyAnHienGoiYTimKiemTraCuu(true)
          }}
        />
        <i
          className="icon icon-nut-x-hinh-tron truong-tac-vu"
          onClick={() => {
            reset({
              ...loc,
              tuKhoa: '',
            })
            xuLyTimKiemTraCuu(prefixLoc)({ tuKhoa: '' })
          }}
        />
        {hienGoiY && <DSGoiYTimKiemTraCuu {...thuocTinhDSGoiYTimKiemTraCuu} />}
      </div>
    </form>
  )
}

TimKiemDiaChi.propTypes = propTypes
TimKiemDiaChi.defaultProps = defaultProps

export const TimKiemToSoThuaSo = (props) => {
  const {
    loc,
    truCP,
    dsQuan,
    dsPhuong,
    dsThanhPho,
    prefixLoc,
    khongSoSanh,
    prefixBanDo,
    dsDiaChiGoiY,
    xuLyTimDiaChiGoiY,
    xuLyTruyCapDSQuan,
    xuLyTruyCapDSPhuong,
    xuLyTimKiemTraCuuChiTietThuaDat,
  } = props
  const {
    reset,
    setValue,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: loc,
  })

  const { maThanhPho, maQuan, maPhuong, tenThanhPho, tenQuan, tenPhuong } = loc || {}

  const duLieuTSTuyChinh = {
    properties: {
      ma_thanh_pho: maThanhPho,
      ma_quan: maQuan,
      ma_phuong: maPhuong,
      thanh_pho: tenThanhPho,
      quan: tenQuan,
      phuong: tenPhuong,
    },
  }

  useEffect(() => {
    reset(loc)
    register('maPhuong', { required: true })
  }, [])

  const thuocTinhChonKhuVuc = {
    dsQuan,
    dsPhuong,
    register,
    setValue,
    dsThanhPho,
    dsDiaChiGoiY,
    xuLyTimDiaChiGoiY,
    xuLyTruyCapDSQuan,
    xuLyTruyCapDSPhuong,
    duLieuTS: duLieuTSTuyChinh,
    moTa: 'Phường/Xã, Quận/Huyện, Tỉnh/Thành phố',
  }

  return (
    <form className="c-tim-kiem-tra-cuu-to-so-thua-so">
      <div className={`truong truong-lop-1 ${truongLoi(errors.maPhuong)}`}>
        <label>
          <i className="icon icon-hinh-tron-trong-vien-vuong" />
          &nbsp;Khu vực tài sản <sup>*</sup>
        </label>
        <ChonKhuVuc {...thuocTinhChonKhuVuc} />
      </div>
      <div className="truong-doi">
        <div className={`truong ${truongLoi(errors.soTo)}`}>
          <label>
            <i className="icon icon-tai-lieu-vien" />
            &nbsp;Tờ bản đồ số <sup>*</sup>
          </label>
          <input type="text" placeholder="Nhập tờ bản đồ số" {...register('soTo', { required: true })} />
        </div>
        <div className={`truong ${truongLoi(errors.soThua)}`}>
          <label>
            <i className="icon icon-tai-lieu-vien" />
            &nbsp;Thửa số <sup>*</sup>
          </label>
          <input type="text" placeholder="Nhập thửa số" {...register('soThua', { required: true })} />
        </div>
      </div>
      <div className="c-bieu-mau-tac-vu">
        <button
          className="nut nut-100-pt"
          onClick={handleSubmit((duLieuBieuMau) => {
            xuLyTimKiemTraCuuChiTietThuaDat({ prefixBanDo, prefixLoc, khongSoSanh, truCP })({
              ...duLieuBieuMau,
              loaiTimKiem: Constants.LOAI_TIM_KIEM.SO_TO_SO_THUA,
            })
          })}
        >
          Tìm kiếm
        </button>
      </div>
    </form>
  )
}

TimKiemToSoThuaSo.propTypes = propTypes
TimKiemToSoThuaSo.defaultProps = defaultProps

export const TimKiemToaDo = (props) => {
  const { loc, truCP, xuLyOCR, prefixLoc, dsThanhPho, prefixBanDo, xuLyTimKiemTraCuuChiTietThuaDat, khongSoSanh } =
    props

  const xy = R.pathOr([{ x: '', y: '' }], ['xy'])(loc)
  const latitude = R.pathOr(0, ['latitude'])(loc)
  const longitude = R.pathOr(0, ['longitude'])(loc)
  const maThanhPho = R.pathOr(0, ['maThanhPho'])(loc)

  const tabTheoToaDoKhoiTao = xy ? Constants.LOAI_TIM_KIEM.VN_2000 : Constants.LOAI_TIM_KIEM.WGS_84
  const [tabTheoToaDo, capNhatTabTheoToaDo] = useState(tabTheoToaDoKhoiTao)
  const timTheoVN2000 = tabTheoToaDo === Constants.LOAI_TIM_KIEM.VN_2000

  const {
    control,
    setValue,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      xy,
      latitude,
      longitude,
      maThanhPho,
    },
  })

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'xy',
  })

  const [dangXyLyHinh, capNhatDangXuLyHinh] = useState(false)
  const dangXuLyDocHinhAnh = dangXyLyHinh
  const choPhepXoaXY = R.length(fields) > 1

  const taoXY = () =>
    fields.map((capXY, i) => (
      <div className="truong-doi-co-tac-vu" key={capXY.id}>
        <div className={`truong ${truongLoi(R.pathOr('', ['xy', i, 'x'])(errors))}`}>
          <label>
            <i className="icon icon-truc-x-vien" />
            &nbsp;Toạ độ X
          </label>
          <input type="number" placeholder="Nhập toạ độ X" {...register(`xy[${i}].x`, { required: true })} />
        </div>
        <div className={`truong ${truongLoi(R.pathOr('', ['xy', i, 'y'])(errors))}`}>
          <label>
            <i className="icon icon-truc-y-vien" />
            &nbsp;Toạ độ Y <sup>*</sup>
          </label>
          <input type="number" placeholder="Nhập toạ độ Y" {...register(`xy[${i}].y`, { required: true })} />
        </div>
        <span className="tac-vu">
          <i
            className={`icon icon-thung-rac-vien ${choPhepXoaXY ? '' : 'icon-vo-hieu'}`}
            onClick={() => {
              choPhepXoaXY && remove(i)
            }}
          />
        </span>
      </div>
    ))

  const thuocTinhTabTheoToaDo = {
    className: 'c-tab-chia-deu',
    xuLyTab: (tab) => {
      capNhatTabTheoToaDo(tab.ma)
    },
    dsTab: [
      {
        ma: Constants.LOAI_TIM_KIEM.WGS_84,
        ten: 'Google',
      },
      {
        ma: Constants.LOAI_TIM_KIEM.VN_2000,
        ten: 'VN2000',
      },
    ],
    tabDangChon: {
      ma: tabTheoToaDo,
    },
    loaiTab: 3,
  }

  const tuyChonDSThanhPho = R.pathOr([], ['duLieu'])(dsThanhPho)

  return (
    <>
      <div className="c-tra-cuu-toa-do">
        <Tab {...thuocTinhTabTheoToaDo} />
      </div>
      {timTheoVN2000 ? (
        <form className="c-tra-cuu-toa-do-vn-2000">
          <div className={`truong truong-lua-chon truong-lop-1 ${truongLoi(errors.tenThanhPho)}`}>
            <label>
              <i className="icon icon-hinh-tron-trong-vien-vuong" />
              &nbsp;Chọn tỉnh thành <sup>*</sup>
            </label>
            <Controller
              control={control}
              name="tenThanhPho"
              rules={{ required: true }}
              render={({ field: { onChange, value } }) => (
                <SelectWrapper
                  placeholder="Chọn tỉnh thành"
                  onChange={(selectedOption) => {
                    const ma = R.pathOr('', ['value'])(selectedOption)
                    const ten = R.pathOr('', ['label'])(selectedOption)

                    onChange(ten)
                    setValue('maThanhPho', ma)
                  }}
                  options={tuyChonDSThanhPho}
                  onInputChange={(v, action) => {
                    R.equals(action.action, 'input-change') && onChange(v)
                  }}
                  value={value ? [{ label: value, value }] : []}
                  isClearable
                  defaultValue={truyCapGiaTriHienTai({ options: tuyChonDSThanhPho, value })}
                />
              )}
            />
          </div>
          {taoXY()}
          <div className="c-bieu-mau-tac-vu c-bieu-mau-tac-vu-2">
            <span
              className="nut nut-100-pt nut-vien nut-vien-xanh nut-do-bong"
              onClick={() =>
                append({
                  x: '',
                  y: '',
                })
              }
            >
              <i className="icon icon-cong" />
              &nbsp;Thêm cặp tọa độ
            </span>

            <span className="nut nut-100-pt nut-vien nut-vien-xanh nut-do-bong nut-dang-tai">
              {dangXuLyDocHinhAnh ? (
                <DangXuLy className="c-dang-xu-ly-inline" />
              ) : (
                <>
                  <i className="icon icon-may-in-vien-1" />
                  &nbsp;Đọc toạ độ bằng tệp ảnh
                </>
              )}
              <input
                multiple={false}
                disabled={dangXuLyDocHinhAnh}
                type="file"
                id="files"
                onChange={(event) => {
                  capNhatDangXuLyHinh(true)
                  xuLyOCR((dsXY) => {
                    remove()
                    dsXY.forEach((xy) => {
                      append(xy)
                    })
                    capNhatDangXuLyHinh(false)
                  })(event)
                }}
                onClick={(event) => {
                  event.target.value = null
                }}
              />
            </span>
          </div>
          <div className="c-bieu-mau-tac-vu">
            <button
              disabled={ktRong(fields)}
              className="nut nut-100-pt"
              onClick={handleSubmit((duLieuBieuMau) => {
                xuLyTimKiemTraCuuChiTietThuaDat({ prefixBanDo, prefixLoc, khongSoSanh, truCP })({
                  ...duLieuBieuMau,
                  loaiTimKiem: Constants.LOAI_TIM_KIEM.VN_2000,
                })
              })}
            >
              Tìm kiếm
            </button>
          </div>
        </form>
      ) : (
        <form>
          <div className="truong-doi">
            <div className={`truong ${truongLoi(errors.latitude)}`}>
              <label>
                <i className="icon icon-vi-do-vien-tron" />
                &nbsp;Vĩ độ <sup>*</sup>
              </label>
              <input type="number" placeholder="Nhập vĩ độ" {...register('latitude', { required: true })} />
            </div>
            <div className={`truong ${truongLoi(errors.longitude)}`}>
              <label>
                <i className="icon icon-kinh-do-vien-tron" />
                &nbsp;Kinh độ <sup>*</sup>
              </label>
              <input type="number" placeholder="Nhập kinh độ" {...register('longitude', { required: true })} />
            </div>
          </div>
          <div className="c-bieu-mau-tac-vu">
            <button
              className="nut nut-100-pt"
              onClick={handleSubmit((duLieuBieuMau) => {
                xuLyTimKiemTraCuuChiTietThuaDat({ prefixBanDo, prefixLoc, khongSoSanh, truCP })({
                  ...duLieuBieuMau,
                  loaiTimKiem: Constants.LOAI_TIM_KIEM.WGS_84,
                })
              })}
            >
              Tìm kiếm
            </button>
          </div>
        </form>
      )}
    </>
  )
}

TimKiemToaDo.propTypes = propTypes
TimKiemToaDo.defaultProps = defaultProps

export default TimKiemTraCuu
