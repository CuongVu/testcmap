import React, { useState, useEffect } from 'react'
import * as R from 'ramda'
import Slider from 'rc-slider'
import Constants from '@constants'
import { DangXuLy } from '@components'
import { ktChuoi } from '@utils/common'
import { ChucNangSuDungDat } from '@ban-do-citics'

import { propTypes, defaultProps } from './BangDieuKhienBanDo.pts'

const BangDieuKhienBanDo = (props) => {
  const {
    loc,
    qtTSCT,
    nhomBanDo,
    prefixBanDo,
    gioiHanZoom,
    qtHSTuongDong,
    chiTietThuaDat,
    chucNangSuDungDat,
    maBangDieuKhienBanDo,
    hienChucNangGia,
    hienChucNangLoc,
    hienChucNangZoom,
    hienChucNangQHXD,
    hienChucNangQHSDD,
    hienChucNangMoRong,
    hienChucNangXemTSSS,
    hienChucNangThuocDo,
    hienChucNangTimTSSS,
    hienChucNangRanhThua,
    hienChucNangLoaiBanDo,
    hienChucNangViTriHienTai,
    xuLyCuaSo,
    xuLyDoMoQHXD,
    xuLyLoaiBanDo,
    xuLyDoMoQHSDD,
    xuLyTatMoQHXD,
    xuLyTatMoQHSDD,
    xuLyLocTrenBanDo,
    xuLyTatMoRanhThua,
    xuLyTimViTriCuaToi,
    xuLyVeDSTSSSTraCuu,
    xuLyTatMoGiaThuaDat,
    xuLyTatMoToanManHinh,
    xuLyTaiThemTSSSDeNghi,
  } = props

  const thuocTinhBanDo = R.pathOr({}, [prefixBanDo])(nhomBanDo)

  const map = R.pathOr(null, ['map'])(thuocTinhBanDo)
  const thuocDoBanDo = R.pathOr(null, ['thuocDoBanDo'])(thuocTinhBanDo)
  const duLieuThuocDoBanDo = R.pathOr({}, ['duLieuThuocDoBanDo'])(thuocTinhBanDo)
  const dangXuLyViTriCuaToi = R.pathOr(false, ['dangXuLyViTriCuaToi'])(thuocTinhBanDo)
  const loaiBanDo = R.pathOr(Constants.MAP.TYPE.ROADMAP, ['loaiBanDo'])(thuocTinhBanDo)

  const doMoQHSDD = R.pathOr(0.9, ['doMoQHSDD'])(thuocTinhBanDo)
  const doMoQHXD = R.pathOr(0.9, ['doMoQHXD'])(thuocTinhBanDo)
  const hienQHSDD = R.pathOr(false, ['hienQHSDD'])(thuocTinhBanDo)
  const hienQHXD = R.pathOr(false, ['hienQHXD'])(thuocTinhBanDo)
  const hienGiaThuaDat = R.pathOr(false, ['hienGiaThuaDat'])(thuocTinhBanDo)
  const hienRanhThua = R.pathOr(false, ['hienRanhThua'])(thuocTinhBanDo)
  const hienToanManHinh = R.pathOr(false, ['hienToanManHinh'])(thuocTinhBanDo)

  const [hienTSSS, anHienTSSS] = useState(false)

  const dsTSSSTraCuu = R.compose(
    R.filter((tsss) => tsss),
    R.pathOr([], ['duLieu', 'compared_assets'])
  )(chiTietThuaDat)

  useEffect(() => {
    if (hienTSSS) {
      anHienTSSS(false)
      hienChucNangXemTSSS &&
        ktChuoi(dsTSSSTraCuu) &&
        xuLyVeDSTSSSTraCuu({
          hienThi: false,
          prefixBanDo,
        })
    }
  }, [hienChucNangXemTSSS, chiTietThuaDat])

  const thuocTinhDoMoQHSDD = {
    min: 0.1,
    max: 1,
    step: 0.1,
    vertical: true,
    defaultValue: 0.9,
    onChange: (doMo) => {
      xuLyDoMoQHSDD({ prefixBanDo, doMo })
    },
  }

  const thuocTinhDoMoQHXD = {
    min: 0.1,
    max: 1,
    step: 0.1,
    vertical: true,
    defaultValue: 0.9,
    onChange: (doMo) => {
      xuLyDoMoQHXD({ prefixBanDo, doMo })
    },
  }

  const thuocTinhChucNangSuDungDat = {
    chucNangSuDungDat,
  }

  return (
    map && (
      <>
        <div className="c-bang-dieu-khien-ban-do" id={maBangDieuKhienBanDo || 'bangDieuKhienBanDo'}>
          <div className="c-bang-dieu-khien-ban-do-phu">
            {hienChucNangMoRong && (
              <span
                className={`nut nut-trang ${hienToanManHinh ? 'dang-chon' : ''}`}
                onClick={() => {
                  xuLyTatMoToanManHinh(prefixBanDo)
                }}
              >
                <i className={`icon icon-${hienToanManHinh ? 'danh-sach-hinh-vuong-tren' : 'bon-huong'}`} />
                &nbsp;{hienToanManHinh ? 'Mở lại danh sách' : 'Mở rộng bản đồ'}
              </span>
            )}
            {hienChucNangTimTSSS && (
              <span
                className="nut nut-trang nut-nho tai-ts-de-nghi-khac"
                onClick={() => {
                  xuLyTaiThemTSSSDeNghi({ qtHSTuongDong, qtTSCT })
                }}
              >
                <i className="icon icon-mui-ten-xoay-nguoc-chieu-dong-ho" />
                &nbsp;{qtHSTuongDong ? 'Tải thêm hồ sơ tương đồng' : 'Tải thêm TSSS khác'}
              </span>
            )}
            {hienChucNangLoc && (
              <span className="nut nut-trang nut-vuong" onClick={xuLyLocTrenBanDo}>
                <i className="icon icon-dieu-chinh" />
              </span>
            )}
          </div>
          <div id="bangDieuKhienBanDoChinh" className="c-bang-dieu-khien-ban-do-chinh">
            {hienChucNangTimTSSS && !qtHSTuongDong && (
              <div className="c-bang-dieu-khien-ban-do-thong-so">
                <span
                  className="nut nut-vuong nut-trang"
                  onClick={() => {
                    xuLyCuaSo({
                      prefix: Constants.CUA_SO.THONG_TIN_BAN_DO_TSSS,
                    })
                  }}
                >
                  <i className="icon icon-cham-than-vien" />
                </span>
                <span className="nut nut-nho nut-opacity">
                  <span>
                    Bán kính bản đồ <strong>{R.pathOr(0, ['radius'])(loc) * 1000}m</strong>
                  </span>
                </span>
              </div>
            )}
            {hienChucNangXemTSSS && (
              <span
                className={`nut nut-vuong nut-trang  ${hienTSSS ? 'dang-chon' : ''}`}
                onClick={() => {
                  xuLyVeDSTSSSTraCuu({
                    hienThi: !hienTSSS,
                    prefixBanDo,
                  })
                  anHienTSSS(!hienTSSS)
                }}
              >
                <i className="icon icon-nhieu-vien-vuong-3d" />
              </span>
            )}
            {hienChucNangTimTSSS && (
              <span
                className={`nut nut-vuong nut-trang ${hienToanManHinh ? 'dang-chon' : ''}`}
                onClick={() => {
                  xuLyTatMoToanManHinh(prefixBanDo)
                }}
              >
                <i className={`icon icon-${hienToanManHinh ? 'nut-x-hinh-tron' : 'bon-huong'}`} />
              </span>
            )}
            {hienChucNangRanhThua && (
              <span
                data-tip={`${hienRanhThua ? 'Ẩn' : 'Xem'} thửa đất`}
                data-html={true}
                className={`nut nut-vuong nut-trang ${hienRanhThua ? 'dang-chon' : ''}`}
                onClick={() => {
                  xuLyTatMoRanhThua(prefixBanDo)
                }}
              >
                <i className="icon icon-lua" />
              </span>
            )}
            {hienChucNangGia && (
              <span
                data-tip={`${hienGiaThuaDat ? 'Ẩn' : 'Xem'} đơn giá thửa đất (tr/m<sup>2</sup>)`}
                data-html={true}
                className={`nut nut-vuong nut-trang ${hienGiaThuaDat ? 'dang-chon' : ''}`}
                onClick={() => {
                  xuLyTatMoGiaThuaDat(prefixBanDo)
                }}
              >
                <i className="icon icon-vnd-vien-tron" />
              </span>
            )}
            {hienChucNangQHSDD && (
              <div className="c-xu-ly-do-mo-quy-hoach">
                {hienQHSDD && (
                  <>
                    <Slider {...thuocTinhDoMoQHSDD} />
                    <span className="c-xu-ly-do-mo-quy-hoach-dang-chon">{doMoQHSDD}</span>
                  </>
                )}
                <span
                  data-tip={`${hienQHSDD ? 'Ẩn' : 'Xem'} quy hoạch sử dụng đất`}
                  className="nut nut-vuong nut-trang"
                  onClick={() => {
                    xuLyTatMoQHSDD(prefixBanDo)
                  }}
                >
                  <i className={`icon icon-${hienQHSDD ? 'nut-x' : 'mui-ten-tach-tren'}`} />
                </span>
              </div>
            )}
            {hienChucNangQHXD && (
              <div className="c-xu-ly-do-mo-quy-hoach">
                {hienQHXD && (
                  <>
                    <Slider {...thuocTinhDoMoQHXD} />
                    <span className="c-xu-ly-do-mo-quy-hoach-dang-chon">{doMoQHXD}</span>
                  </>
                )}
                <span
                  data-tip={`${hienQHXD ? 'Ẩn' : 'Xem'} quy hoạch xây dựng`}
                  className="nut nut-vuong nut-trang"
                  onClick={() => {
                    xuLyTatMoQHXD(prefixBanDo)
                  }}
                >
                  <i className={`icon icon-${hienQHXD ? 'nut-x' : 'mot-lop-hinh-hai-lop-vien'}`} />
                </span>
              </div>
            )}
            {hienChucNangViTriHienTai && (
              <span
                data-tip="Vị trí hiện tại"
                className="nut nut-vuong nut-trang vi-tri-hien-tai"
                onClick={() => {
                  xuLyTimViTriCuaToi(prefixBanDo)
                }}
              >
                {dangXuLyViTriCuaToi ? (
                  <DangXuLy className="c-dang-xu-ly-inline" />
                ) : (
                  <i className="icon icon-dinh-huong-hinh" />
                )}
              </span>
            )}
            {hienChucNangThuocDo && (
              <>
                {R.pathOr(false, ['isStart'])(duLieuThuocDoBanDo) ? (
                  <span
                    className="nut nut-vuong nut-trang"
                    id="end"
                    onClick={() => {
                      // không được truyền event vào ()
                      thuocDoBanDo.end()
                    }}
                  >
                    <i className="icon icon-huy-bo-vien" />
                  </span>
                ) : (
                  <span
                    data-tip="Đo khoảng cách"
                    className="nut nut-vuong nut-trang"
                    id="start-with-points"
                    onClick={() => {
                      // không được truyền event vào ()
                      thuocDoBanDo.start()
                    }}
                  >
                    <i className="icon icon-thuoc-do" />
                  </span>
                )}
              </>
            )}
            {hienChucNangZoom && (
              <>
                <span
                  data-tip="Phóng to"
                  className="nut nut-vuong nut-trang nut-zoom-in"
                  onClick={() => {
                    const zoomDangChon = map && map.getZoom()
                    const zoomMoi = zoomDangChon + 1

                    map && map.setZoom(zoomMoi)
                  }}
                >
                  <i className="icon icon-cong" />
                </span>
                <span
                  data-tip="Thu nhỏ"
                  className="nut nut-vuong nut-trang nut-zoom-out"
                  onClick={() => {
                    const zoomDangChon = map && map.getZoom()
                    const zoomMoi = zoomDangChon - 1

                    if (!gioiHanZoom || R.gte(zoomMoi, Constants.MAP.ZOOM.MAX_OUT)) {
                      map && map.setZoom(zoomMoi)
                    }
                  }}
                >
                  <i className="icon icon-dau-tru" />
                </span>
              </>
            )}
            {hienChucNangLoaiBanDo && (
              <>
                <span
                  data-tip="Xem bản đồ mặc định"
                  onClick={() => {
                    xuLyLoaiBanDo({ loaiBanDo: Constants.MAP.TYPE.ROADMAP, prefixBanDo })
                  }}
                  className={`nut nut-vuong nut-trang loai-ban-do ${
                    loaiBanDo === Constants.MAP.TYPE.ROADMAP ? 'dang-chon' : ''
                  }`}
                >
                  <i className="icon icon-ban-do-hinh" />
                </span>
                <span
                  data-tip="Xem bản đồ vệ tinh"
                  onClick={() => {
                    xuLyLoaiBanDo({ loaiBanDo: Constants.MAP.TYPE.SATELLITE, prefixBanDo })
                  }}
                  className={`nut nut-vuong nut-trang ve-tinh ${
                    loaiBanDo === Constants.MAP.TYPE.SATELLITE ? 'dang-chon' : ''
                  }`}
                >
                  <i className="icon icon-ve-tinh" />
                </span>
              </>
            )}
          </div>
        </div>
        {hienQHSDD && <ChucNangSuDungDat {...thuocTinhChucNangSuDungDat} />}
      </>
    )
  )
}

BangDieuKhienBanDo.propTypes = propTypes
BangDieuKhienBanDo.defaultProps = defaultProps

export default BangDieuKhienBanDo
