/* eslint-disable camelcase */
import React, { useEffect } from 'react'
import * as R from 'ramda'
import { Polygon, GoogleMap, useJsApiLoader } from '@react-google-maps/api'
import Constants from '@constants'
import { DangXuLy, Logo } from '@components'

import { propTypes, defaultProps } from './BanDo.pts'

const libraries = ['places', 'geometry', 'drawing']

const BanDo = (props) => {
  const {
    tsChinh,
    nhomBanDo,
    prefixBanDo,
    khoiTaoBanDo,
    capNhatNhomBanDo,
    thuocTinhBanDoTuyChinh,
    thuocTinhGoogleMapTuyChinh,
    xuLyZoom,
    xuLyBounds,
    xuLyThayDoi,
    xuLyDiChuyen,
    xuLyNhapChuot,
  } = props

  const { isLoaded } = useJsApiLoader({
    googleMapsApiKey: Constants.MAP.KEY,
    id: 'google-map-script',
    libraries,
    region: 'VN',
    language: 'vi_VN',
  })

  const thuocTinhBanDo = R.pathOr({}, [prefixBanDo])(nhomBanDo)
  const duLieuThuocDoBanDo = R.pathOr({}, ['duLieuThuocDoBanDo'])(thuocTinhBanDo)
  const loaiBanDo = R.pathOr(Constants.MAP.TYPE.ROADMAP, ['loaiBanDo'])(thuocTinhBanDo)
  const hienQHSDD = R.pathOr(false, ['hienQHSDD'])(thuocTinhBanDo)
  const anRanhThua = R.pathOr(false, ['anRanhThua'])(thuocTinhBanDo)
  const chiTietThuaDat = R.pathOr({}, ['chiTietThuaDat'])(thuocTinhBanDo)
  const duLieuTSTam = R.pathOr({}, ['chiTietTSTam', 'duLieu'])(thuocTinhBanDo)

  useEffect(() => {
    return () => {
      capNhatNhomBanDo({
        prefixBanDo,
        thuocTinhBanDo: {
          hienQHSDD: false,
          dsRanhThuaDaVe: [],
          dsQuyHoachDaVe: [],
          duLieuDSRanhThuaDaVe: [],
          duLieuDSQuyHoachDaVe: [],
        },
      })
    }
  }, [])

  const taoBanDo = () => {
    const laVeTinh = R.equals(loaiBanDo, Constants.MAP.TYPE.SATELLITE)
    const batDauDo = R.pathOr(false, ['isStart'])(duLieuThuocDoBanDo)
    const thuocTinhVeRanhThua = {
      ...Constants.MAP.DEFAULT.POLYGON,
      clickable: false,
      draggable: false,
      editable: false,
      fillOpacity: hienQHSDD ? 0 : laVeTinh ? 1 : 0.7,
      fillColor: laVeTinh ? '#e0e0e0' : '#747576',
      radius: 30000,
      visible: true,
      zIndex: 9999,
      strokeOpacity: hienQHSDD ? 1 : 0,
      strokeWeight: hienQHSDD ? 2 : 1,
    }

    const thuocTinhThuaDat = R.pathOr({}, ['duLieu', 'properties'])(chiTietThuaDat)
    const ranhThuaDangChon = R.pathOr(null, ['ranh_gioi'])(thuocTinhThuaDat)
    const ranhThuaDaTach = R.pathOr(null, ['ranh_gioi_wgs84'])(thuocTinhThuaDat)
    // const trangThaiThuaDat = R.pathOr('', ['trang_thai_thua_dat', 'key'])(thuocTinhThuaDat)

    const thuocTinhTSChinh = R.pathOr({}, ['duLieu', 'properties'])(tsChinh)
    const ranhThuaTSChinh = R.pathOr(null, ['ranh_gioi'])(thuocTinhTSChinh)
    const ranhThuaTSChinhDaTach = R.pathOr(null, ['ranh_gioi_wgs84'])(thuocTinhTSChinh)
    // const trangThaiThuaDatTSChinh = R.pathOr('', ['trang_thai_thua_dat', 'key'])(thuocTinhTSChinh)

    const thuocTinhTSTam = R.pathOr({}, ['properties'])(duLieuTSTam)
    const ranhThuaTSTam = R.pathOr(null, ['ranh_gioi'])(thuocTinhTSTam)
    const ranhThuaTSTamDaTach = R.pathOr(null, ['ranh_gioi_wgs84'])(thuocTinhTSTam)
    // const trangThaiThuaDatTSTam = R.pathOr('', ['trang_thai_thua_dat', 'key'])(thuocTinhTSTam)

    // Này là thuộc tính gốc của google map
    const thuocTinhBanDo = {
      disableDoubleClickZoom: false,
      fullscreenControl: false,
      gestureHandling: 'greedy',
      mapTypeControl: false,
      // scrollwheel: false,
      styles: Constants.MAP.STYLE.DEFAULT,
      zoomControl: false,
      restriction: {
        latLngBounds: {
          east: 180,
          north: 85,
          south: -85,
          west: -180,
        },
        strictBounds: true,
      },
      ...thuocTinhBanDoTuyChinh,
    }

    // Này là thuộc tính của plugin
    const thuocTinhGoogleMap = {
      id: 'citics-map',
      clickableIcons: false,
      center: Constants.MAP.DEFAULT.CENTER,
      mapTypeId: Constants.MAP.TYPE.ROADMAP,
      ...thuocTinhGoogleMapTuyChinh,
      zoom: Constants.MAP.ZOOM.DEFAULT,
    }

    const veRanhThuaDangChon = () => {
      if (!anRanhThua) {
        if (ranhThuaDangChon) {
          const path = ranhThuaDangChon.map((toaDo) => ({
            lat: toaDo[1] || 0,
            lng: toaDo[0] || 0,
          }))

          return <Polygon paths={path} options={{ ...thuocTinhVeRanhThua, fillOpacity: 0.5 }} />
        }
      }
    }

    const veRanhThuaTSChinh = () => {
      if (!anRanhThua) {
        if (ranhThuaTSChinh) {
          const path = ranhThuaTSChinh.map((toaDo) => ({
            lat: toaDo[1] || 0,
            lng: toaDo[0] || 0,
          }))

          return <Polygon paths={path} options={{ ...thuocTinhVeRanhThua }} />
        }
      }
    }

    const veRanhThuaTSTam = () => {
      if (!anRanhThua) {
        if (ranhThuaTSTam) {
          const path = ranhThuaTSTam.map((toaDo) => ({
            lat: toaDo[1] || 0,
            lng: toaDo[0] || 0,
          }))

          return <Polygon paths={path} options={{ ...thuocTinhVeRanhThua, fillOpacity: 0.5 }} />
        }
      }
    }

    const veRanhThuaDaTach = () => {
      if (ranhThuaDaTach) {
        const path = ranhThuaDaTach.map((toaDo) => ({
          lat: toaDo[1] || 0,
          lng: toaDo[0] || 0,
        }))

        return <Polygon paths={path} options={{ ...thuocTinhVeRanhThua, strokeColor: '#649607', fillOpacity: 0.5 }} />
      }
    }

    const veRanhThuaTSChinhDaTach = () => {
      if (ranhThuaTSChinhDaTach) {
        const path = ranhThuaTSChinhDaTach.map((toaDo) => ({
          lat: toaDo[1] || 0,
          lng: toaDo[0] || 0,
        }))

        return <Polygon paths={path} options={{ ...thuocTinhVeRanhThua, strokeColor: '#649607', fillOpacity: 0.5 }} />
      }
    }

    const veRanhThuaTSTamDaTach = () => {
      if (ranhThuaTSTamDaTach) {
        const path = ranhThuaTSTamDaTach.map((toaDo) => ({
          lat: toaDo[1] || 0,
          lng: toaDo[0] || 0,
        }))

        return <Polygon paths={path} options={{ ...thuocTinhVeRanhThua, strokeColor: '#649607', fillOpacity: 0.5 }} />
      }
    }

    return (
      <GoogleMap
        {...thuocTinhGoogleMap}
        onClick={(e) => {
          !batDauDo && xuLyNhapChuot(e)
        }}
        onLoad={khoiTaoBanDo}
        onIdle={xuLyThayDoi}
        options={thuocTinhBanDo}
        onDragEnd={xuLyDiChuyen}
        onZoomChanged={xuLyZoom}
        onBoundsChanged={xuLyBounds}
        mapContainerClassName="c-ban-do"
      >
        {veRanhThuaDaTach()}
        {veRanhThuaDangChon()}
        {veRanhThuaTSChinh()}
        {veRanhThuaTSChinhDaTach()}
        {veRanhThuaTSTam()}
        {veRanhThuaTSTamDaTach()}
        <div className="c-logo-map">
          <Logo />
        </div>
      </GoogleMap>
    )
  }

  return isLoaded ? taoBanDo() : <DangXuLy className="c-dang-xu-ly-inline" />
}

BanDo.propTypes = propTypes
BanDo.defaultProps = defaultProps

export default BanDo
