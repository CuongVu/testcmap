import PropTypes from 'prop-types'

export const propTypes = {
  xuLyTimKiemTraCuuDSDuAn: PropTypes.func,
}

export const defaultProps = {
  xuLyTimKiemTraCuuDSDuAn: () => {},
}
