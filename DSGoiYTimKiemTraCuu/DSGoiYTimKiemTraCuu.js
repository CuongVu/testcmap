/* eslint-disable camelcase */
import React, { useState, useEffect } from 'react'
import * as R from 'ramda'
import Constants from '@constants'
import { ktChuoi } from '@utils/common'
import { DangXuLy } from '@components'

import { propTypes, defaultProps } from './DSGoiYTimKiemTraCuu.pts'

const DSGoiYTimKiemTraCuu = (props) => {
  const {
    loc,
    truCP,
    prefixLoc,
    prefixBanDo,
    khongSoSanh,
    dongTimKiemDuAn,
    dongTimKiemNhaDat,
    dsGoiYTimKiemTraCuu,
    xuLyTimKiemTraCuuDuAn,
    xuLyTimKiemTraCuuDSDuAn,
    xuLyTimKiemTraCuuChiTietThuaDat,
  } = props
  const [sttGoiYDangChon, capNhatSTTGoiYDangChon] = useState(0)
  const { loaiTimKiem, tuKhoa } = loc || {}
  const dangXuLy = R.pathOr(false, ['dangXuLy'])(dsGoiYTimKiemTraCuu)
  const loaiTimKiemDuAn = R.includes(loaiTimKiem, [
    Constants.LOAI_TIM_KIEM.CAN_HO,
    Constants.LOAI_TIM_KIEM.DU_AN,
    Constants.LOAI_TIM_KIEM.DS_DU_AN,
  ])
  const _DSGoiYTimKiemTraCuu = [
    ...(loaiTimKiemDuAn
      ? [
          {
            source: Constants.LOAI_TIM_KIEM.DS_DU_AN,
          },
        ]
      : []),
    ...R.pathOr([], ['duLieu', 'items'])(dsGoiYTimKiemTraCuu),
  ]
  const coDuLieu = ktChuoi(_DSGoiYTimKiemTraCuu)
  const soLuongGoiY = R.length(_DSGoiYTimKiemTraCuu)
  const xuLyPhim = (e) => {
    if (e.keyCode === 40) {
      // key down
      capNhatSTTGoiYDangChon(sttGoiYDangChon >= soLuongGoiY - 1 ? 0 : sttGoiYDangChon + 1)
    } else if (e.keyCode === 38) {
      // key up
      capNhatSTTGoiYDangChon(sttGoiYDangChon === 0 ? soLuongGoiY - 1 : sttGoiYDangChon - 1)
    } else if (e.keyCode === 13) {
      // key enter
      if (coDuLieu && R.gt(sttGoiYDangChon, -1)) {
        // không sử dụng biểu mẫu
        e.preventDefault()

        const goiYDangChon = R.pathOr({}, [sttGoiYDangChon])(_DSGoiYTimKiemTraCuu)

        const { extra: { latitude, longitude } = {}, source, id } = goiYDangChon
        const timKiemDSDuAn = source === Constants.LOAI_TIM_KIEM.DS_DU_AN

        if (timKiemDSDuAn) {
          dongTimKiemDuAn()
          xuLyTimKiemTraCuuDSDuAn({ prefixBanDo, prefixLoc })({
            tuKhoa,
            latitude,
            longitude,
            maDuAn: id,
            fitBounds: true,
            loaiTimKiem: Constants.LOAI_TIM_KIEM.DS_DU_AN,
          })
        } else {
          if (loaiTimKiemDuAn) {
            dongTimKiemDuAn()
            xuLyTimKiemTraCuuDuAn({ prefixBanDo, prefixLoc })({
              tuKhoa,
              latitude,
              longitude,
              maDuAn: id,
              fitBounds: false,
              loaiTimKiem: Constants.LOAI_TIM_KIEM.DU_AN,
            })
          } else {
            dongTimKiemNhaDat()
            !dangXuLy &&
              xuLyTimKiemTraCuuChiTietThuaDat({ prefixBanDo, prefixLoc, khongSoSanh, truCP })({
                maTS: id,
                latitude,
                longitude,
                loaiTimKiem,
                laGoogle: source === Constants.LOAI_TIM_KIEM.GOOGLE,
              })
          }
        }
      } else {
        // do something
      }
    }
  }

  useEffect(() => {
    window.addEventListener('keydown', xuLyPhim)

    return () => {
      window.removeEventListener('keydown', xuLyPhim)
    }
  }, [xuLyPhim])

  return (
    <div className="ds-goi-y-tim-kiem-tra-cuu">
      {dangXuLy || !dsGoiYTimKiemTraCuu ? (
        <ul>
          <li>
            <DangXuLy className="c-dang-xu-ly-inline" />
          </li>
        </ul>
      ) : !coDuLieu ? (
        <ul>
          <li>
            Rất tiếc!
            <br />
            Không thể tìm địa chỉ tài sản này trong hệ thống.
            <br />
            Bạn có thể thử nhập từ khóa tìm kiếm khác, hoặc chọn
            <br />
            <span className="lien-ket">
              <i className="icon icon-cong-vien-tron" />
              &nbsp;Thêm mới tài sản
            </span>
          </li>
        </ul>
      ) : (
        <ul>
          {coDuLieu &&
            _DSGoiYTimKiemTraCuu.map((goiY, i) => {
              const { extra: { latitude, longitude } = {}, source, text, sub_text, id } = goiY
              const timKiemDSDuAn = source === Constants.LOAI_TIM_KIEM.DS_DU_AN

              return (
                <li
                  key={i}
                  className={`${i === sttGoiYDangChon ? 'dang-chon' : ''} ${
                    timKiemDSDuAn ? 'tim-du-an-theo-vung' : ''
                  }`}
                  onClick={(e) => e.stopPropagation()}
                >
                  {timKiemDSDuAn ? (
                    <div
                      onClick={() => {
                        dongTimKiemDuAn()
                        xuLyTimKiemTraCuuDSDuAn({ prefixBanDo, prefixLoc })({
                          loaiTimKiem,
                          latitude,
                          longitude,
                          maDuAn: id,
                          tuKhoa,
                          fitBounds: true,
                        })
                      }}
                    >
                      Xem các dự án <strong>{R.trim(tuKhoa)}</strong>
                    </div>
                  ) : (
                    <div
                      onClick={() => {
                        if (loaiTimKiemDuAn) {
                          dongTimKiemDuAn()
                          xuLyTimKiemTraCuuDuAn({ prefixBanDo, prefixLoc })({
                            loaiTimKiem: Constants.LOAI_TIM_KIEM.DU_AN,
                            latitude,
                            longitude,
                            maDuAn: id,
                            tuKhoa,
                            fitBounds: false,
                          })
                        } else {
                          dongTimKiemNhaDat()
                          xuLyTimKiemTraCuuChiTietThuaDat({ prefixBanDo, prefixLoc, khongSoSanh, truCP })({
                            loaiTimKiem,
                            latitude,
                            longitude,
                            maTS: id,
                            laGoogle: source === Constants.LOAI_TIM_KIEM.GOOGLE,
                          })
                        }
                      }}
                    >
                      <strong>{text}</strong>
                      <br />
                      <small>{sub_text}</small>
                    </div>
                  )}
                </li>
              )
            })}
        </ul>
      )}
    </div>
  )
}

DSGoiYTimKiemTraCuu.propTypes = propTypes
DSGoiYTimKiemTraCuu.defaultProps = defaultProps

export default DSGoiYTimKiemTraCuu
