# Bản đồ Citics

## Giới thiệu

Các chức năng liên quan đến bản đồ Citics bao gồm:
- Tra cứu tài sản
- Quản lý vị trí tài sản

## Hướng dẫn sử dụng

- Hoạt động theo cách thức là 1 submodule trong 1 dự án

## Hướng dẫn thêm submodule vào dự án
Chỉ sử dụng khi là người đầu tiên tạo submodule mới cho 1 dự án.
Nếu dự án đã có submodule thì bỏ qua phần này.
```
cd project-folder
git submodule add https://gitlab.com/CuongVu/ban-do-citics.git src/ban-do-citics
git submodule init
git submodule update
```

## Hướng dẫn lấy submodule về máy
Khi dự án có kết nối với các submodule thì khi lấy về chỉ cần init và update:
```
cd project-folder
git pull
git submodule init
git submodule update
```

## Hướng dẫn add, commit và push thay đổi của submodule
Khi chạy những lệnh sau, những thay đổi sẽ được đưa lên git của submodule, để các thành viên khác có thể pull về.
```
cd project-folder
git submodule foreach git add .
git submodule foreach git commit -m 'Ghi chú chung những thay đổi của các submodule'
git submodule foreach git push
```
Sau đó cập nhật cho git dự án:
```
git add .
git commit -m 'Ghi chú thay đổi của dự án'
git push origin HEAD
```

## Sử dụng alias nếu cần
Ta có thể cấu hình alias như sau:
```
git config alias.sadd 'submodule foreach git add .'
git config alias.scommit "submodule foreach git commit -m '<Tên> thay đổi submodule'"
git config alias.spush 'submodule foreach git push'
```
Sau đó chỉ cần chạy: 
```
git sadd
git scommit
git spush
```
Sau đó cập nhật cho git dự án:
```
git add .
git commit -m 'Ghi chú thay đổi của dự án'
git push origin HEAD
```

## Xem thêm đầy đủ thông tin về submodule tại:
https://git-scm.com/book/en/v2/Git-Tools-Submodules