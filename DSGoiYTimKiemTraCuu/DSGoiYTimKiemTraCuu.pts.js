import PropTypes from 'prop-types'

export const propTypes = {
  dongTimKiemDuAn: PropTypes.func,
  dongTimKiemNhaDat: PropTypes.func,
  xuLyTimKiemTraCuuDSDuAn: PropTypes.func,
}

export const defaultProps = {
  dongTimKiemDuAn: () => {},
  dongTimKiemNhaDat: () => {},
  xuLyTimKiemTraCuuDSDuAn: () => {},
}
