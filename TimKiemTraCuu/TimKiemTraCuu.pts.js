import PropTypes from 'prop-types'

export const propTypes = {
  className: PropTypes.string,
  xuLyTimKiemTraCuu: PropTypes.func,
  xuLyTimKiemTraCuuChiTietThuaDat: PropTypes.func,
}

export const defaultProps = {
  className: '',
  xuLyTimKiemTraCuu: () => {},
  xuLyTimKiemTraCuuChiTietThuaDat: () => {},
}
