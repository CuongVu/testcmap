/* eslint-disable camelcase */
import React, { useState, useEffect, Fragment } from 'react'
import * as R from 'ramda'
// import Link from 'next/link'
import Router from 'next/router'
import Constants from '@constants'
import { DangXuLy } from '@components'
import { ktChuoi, taoThongSoDuongDan } from '@utils/common'

import { propTypes, defaultProps } from './DSGoiYTimKiemTraCuuChung.pts'

const DSGoiYTimKiemTraCuuChung = (props) => {
  const { loc, dsGoiYTimKiemTraCuuChung, hoSoCaNhan } = props
  const maNguoiDung = R.pathOr('', ['duLieu', 'id'])(hoSoCaNhan)
  const [sttGoiYDangChon, capNhatSTTGoiYDangChon] = useState(0)
  const { tuKhoa } = loc || {}
  const dangXuLy = R.pathOr(false, ['dangXuLy'])(dsGoiYTimKiemTraCuuChung)
  const _DSGoiYTimKiemTraCuu = R.compose(
    R.sortWith([R.ascend(R.prop('priority'))]),
    R.pathOr([], ['duLieu', 'danhSach'])
  )(dsGoiYTimKiemTraCuuChung)

  const dsGoiYTong = R.compose(R.flatten, R.map(R.prop('items')))(_DSGoiYTimKiemTraCuu)

  const coDuLieu = ktChuoi(_DSGoiYTimKiemTraCuu)
  const soLuongGoiY = R.length(dsGoiYTong)
  const xuLyPhim = (e) => {
    if (e.keyCode === 40) {
      // key down
      capNhatSTTGoiYDangChon(sttGoiYDangChon >= soLuongGoiY - 1 ? 0 : sttGoiYDangChon + 1)
    } else if (e.keyCode === 38) {
      // key up
      capNhatSTTGoiYDangChon(sttGoiYDangChon === 0 ? soLuongGoiY - 1 : sttGoiYDangChon - 1)
    } else if (e.keyCode === 13) {
      // key enter
      if (coDuLieu && R.gt(sttGoiYDangChon, -1)) {
        // không sử dụng biểu mẫu
        e.preventDefault()

        const goiYDangChon = R.pathOr({}, [sttGoiYDangChon])(dsGoiYTong)

        const { extra: { latitude, longitude } = {}, source, id, text } = goiYDangChon
        const loaiTimKiemDSDuAn = source === Constants.LOAI_TIM_KIEM.DS_DU_AN
        const loaiTimKiemDuAn = R.includes(source, [Constants.LOAI_TIM_KIEM.CAN_HO, Constants.LOAI_TIM_KIEM.DU_AN])
        const loaiTimKiemGoogle = source === Constants.LOAI_TIM_KIEM.GOOGLE

        const thongSo = loaiTimKiemDSDuAn
          ? `?${taoThongSoDuongDan({
              loaiTimKiem: Constants.LOAI_TIM_KIEM.DS_DU_AN,
              page: 0,
              tuKhoa,
              tenTS: text,
            })}`
          : loaiTimKiemDuAn
          ? `?${taoThongSoDuongDan({
              loaiTimKiem: Constants.LOAI_TIM_KIEM.DS_DU_AN,
              tuKhoa,
              tenTS: text,
              maDuAn: id,
            })}`
          : loaiTimKiemGoogle
          ? `?${taoThongSoDuongDan({
              loaiTimKiem: Constants.LOAI_TIM_KIEM.GOOGLE,
              longitude,
              latitude,
              maTS: id,
            })}`
          : `?${taoThongSoDuongDan({
              loaiTimKiem: Constants.LOAI_TIM_KIEM.NHA_DAT,
              longitude,
              latitude,
            })}`

        const duongDan = maNguoiDung
          ? `${Constants.TRANG.TRA_CUU}${thongSo}`
          : `${Constants.TRANG.DANG_NHAP}${thongSo}&dangTraCuu=1`

        if (loaiTimKiemDSDuAn) {
          Router.push(duongDan)
        } else {
          if (loaiTimKiemDuAn) {
            Router.push(duongDan)
          } else {
            loaiTimKiemGoogle ? Router.push(duongDan) : Router.push(duongDan)
          }
        }
      } else {
        // do something
      }
    }
  }

  useEffect(() => {
    window.addEventListener('keydown', xuLyPhim)

    return () => {
      window.removeEventListener('keydown', xuLyPhim)
    }
  }, [xuLyPhim])

  let sttKetQua = 0

  return (
    <div className="ds-goi-y-tim-kiem-tra-cuu-chung">
      {dangXuLy || !dsGoiYTimKiemTraCuuChung ? (
        <ul>
          <li>
            <DangXuLy className="c-dang-xu-ly-inline" />
          </li>
        </ul>
      ) : !coDuLieu ? (
        <ul>
          <li>
            Rất tiếc!
            <br />
            Không thể tìm địa chỉ tài sản này trong hệ thống.
            <br />
            Bạn có thể thử nhập từ khóa tìm kiếm khác.
          </li>
        </ul>
      ) : (
        <ul>
          {coDuLieu &&
            _DSGoiYTimKiemTraCuu.map((nhomGoiY, y) => {
              const { items, label, name } = nhomGoiY || {}

              const icon =
                name === Constants.LOAI_TIM_KIEM.NHA_DAT
                  ? 'ngoi-nha-vien'
                  : name === Constants.LOAI_TIM_KIEM.DU_AN
                  ? 'can-ho-vien'
                  : name === Constants.LOAI_TIM_KIEM.GOOGLE
                  ? 'xac-dinh-vi-tri-vien'
                  : ''

              return (
                <Fragment key={y}>
                  <li className="ds-goi-y-tim-kiem-tra-cuu-chung-tieu-de">
                    <i className={`icon icon-${icon}`} />
                    &nbsp;<strong>{label}</strong>
                  </li>
                  {ktChuoi(items) &&
                    items.map((goiY, i) => {
                      const { extra: { latitude, longitude } = {}, source, text, sub_text, id } = goiY
                      const loaiTimKiemDSDuAn = source === Constants.LOAI_TIM_KIEM.DS_DU_AN
                      const loaiTimKiemDuAn = R.includes(source, [
                        Constants.LOAI_TIM_KIEM.CAN_HO,
                        Constants.LOAI_TIM_KIEM.DU_AN,
                      ])
                      const loaiTimKiemGoogle = source === Constants.LOAI_TIM_KIEM.GOOGLE

                      sttKetQua = sttKetQua + 1

                      const thongSo = loaiTimKiemDSDuAn
                        ? `?${taoThongSoDuongDan({
                            loaiTimKiem: Constants.LOAI_TIM_KIEM.DS_DU_AN,
                            page: 0,
                            tuKhoa,
                            tenTS: text,
                          })}`
                        : loaiTimKiemDuAn
                        ? `?${taoThongSoDuongDan({
                            loaiTimKiem: Constants.LOAI_TIM_KIEM.DS_DU_AN,
                            tuKhoa,
                            tenTS: text,
                            maDuAn: id,
                          })}`
                        : loaiTimKiemGoogle
                        ? `?${taoThongSoDuongDan({
                            loaiTimKiem: Constants.LOAI_TIM_KIEM.GOOGLE,
                            longitude,
                            latitude,
                            maTS: id,
                          })}`
                        : `?${taoThongSoDuongDan({
                            loaiTimKiem: Constants.LOAI_TIM_KIEM.NHA_DAT,
                            longitude,
                            latitude,
                          })}`

                      const duongDan = maNguoiDung
                        ? `${Constants.TRANG.TRA_CUU}${thongSo}`
                        : `${Constants.TRANG.DANG_NHAP}${thongSo}&dangTraCuu=1`

                      return (
                        <li
                          key={i}
                          className={`${sttKetQua === sttGoiYDangChon + 1 ? 'dang-chon' : ''} ${
                            loaiTimKiemDSDuAn ? 'tim-du-an-theo-vung' : ''
                          }`}
                          onClick={(e) => e.stopPropagation()}
                        >
                          {loaiTimKiemDSDuAn ? (
                            <div
                              onClick={() => {
                                Router.push(duongDan)
                              }}
                            >
                              Xem các dự án <strong>{R.trim(tuKhoa)}</strong>
                            </div>
                          ) : (
                            <div
                              onClick={() => {
                                if (loaiTimKiemDuAn) {
                                  Router.push(duongDan)
                                } else {
                                  loaiTimKiemGoogle ? Router.push(duongDan) : Router.push(duongDan)
                                }
                              }}
                            >
                              <strong>{text}</strong>
                              <br />
                              <small>{sub_text}</small>
                            </div>
                          )}
                        </li>
                      )
                    })}
                </Fragment>
              )
            })}
        </ul>
      )}
    </div>
  )
}

DSGoiYTimKiemTraCuuChung.propTypes = propTypes
DSGoiYTimKiemTraCuuChung.defaultProps = defaultProps

export default DSGoiYTimKiemTraCuuChung
