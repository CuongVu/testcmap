import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import * as R from 'ramda'
import Constants from '@constants'
import { ktLoaiTSTheoLoaiTimKiem } from '@utils/common'
import { DSGoiYTimKiemTraCuuChung } from '@ban-do-citics'

import { propTypes, defaultProps } from './TimKiemTraCuuChung.pts'

const nhomTimeout = {}

const TimKiemTraCuuChung = (props) => {
  const { chiTimCanHo, chiTimNhaDat, loc = {}, maTimKiemTraCuu, prefixLoc, prefixBanDo } = props

  useEffect(() => {
    chiTimCanHo && capNhatLoaiTimKiem(Constants.LOAI_TAI_SAN.CAN_HO)
    chiTimNhaDat && capNhatLoaiTimKiem(Constants.LOAI_TAI_SAN.NHA_DAT)
  }, [chiTimCanHo, chiTimNhaDat])

  const [loaiTimKiem, capNhatLoaiTimKiem] = useState(loc.loaiTimKiem || Constants.LOAI_TIM_KIEM.NHA_DAT)

  const ktLoaiTS = ktLoaiTSTheoLoaiTimKiem(loaiTimKiem)

  const loaiTimKiemNhaDat = R.pathOr(false, ['traCuuNhaDat'])(ktLoaiTS)

  const [hienTimKiemNhaDat, anHienTimKiemNhaDat] = useState(loaiTimKiemNhaDat)

  const dongTimKiemNhaDat = () => {
    anHienTimKiemNhaDat(false)
  }

  useEffect(() => {
    document.addEventListener('click', dongTimKiemNhaDat)

    return () => {
      document.removeEventListener('click', dongTimKiemNhaDat)
    }
  }, [])

  useEffect(() => {
    const loaiTimKiem = loc.loaiTimKiem || Constants.LOAI_TIM_KIEM.NHA_DAT

    capNhatLoaiTimKiem(loaiTimKiem)
  }, [loc])

  const thuocTinhTimKiemDiaChiChung = {
    ...props,
    prefixLoc,
    loaiTimKiem,
    prefixBanDo,
  }

  return (
    <div
      id={maTimKiemTraCuu || 'timKiemTraCuu'}
      className={`c-tim-kiem-tra-cuu-chung ${hienTimKiemNhaDat ? 'dang-mo' : 'dang-dong'}`}
      onClick={(e) => {
        e.stopPropagation()
      }}
    >
      <TimKiemDiaChiChung {...thuocTinhTimKiemDiaChiChung} />
    </div>
  )
}

TimKiemTraCuuChung.propTypes = propTypes
TimKiemTraCuuChung.defaultProps = defaultProps

export const TimKiemDiaChiChung = (props) => {
  // Tìm nhà đất & dự án theo từ khoá
  const {
    loc,
    prefixLoc,
    hoSoCaNhan,
    prefixBanDo,
    khongSoSanh,
    loaiTimKiem,
    className = '',
    dongTimKiemDuAn,
    dongTimKiemNhaDat,
    xuLyTimKiemTraCuuChung,
    dsGoiYTimKiemTraCuuChung,
  } = props
  const { register, setFocus, reset } = useForm({
    defaultValues: loc,
  })
  const [hienGoiY, anHienGoiY] = useState(false)

  useEffect(() => {
    setFocus('tuKhoa', { shouldSelect: true })
  }, [setFocus])

  useEffect(() => {
    nhomTimeout.traCuuNhaDat = null

    xuLyTimKiemTraCuuChung(prefixLoc)({
      loaiTimKiem,
      tuKhoa: R.pathOr('', ['tuKhoa'])(loc),
    })

    return () => {
      xuLyAnHienGoiYTimKiemTraCuu(false)
    }
  }, [])

  const xuLyAnHienGoiYTimKiemTraCuu = (trangThai) => {
    anHienGoiY(trangThai)
  }

  const thuocTinhDSGoiYTimKiemTraCuuChung = {
    loc,
    hoSoCaNhan,
    khongSoSanh,
    dongTimKiemDuAn,
    dongTimKiemNhaDat,
    dsGoiYTimKiemTraCuuChung,
    prefixBanDo,
    prefixLoc,
  }

  return (
    <form
      className={className}
      onSubmit={(e) => {
        e.preventDefault()
      }}
    >
      <div className="truong truong-co-tac-vu truong-co-bieu-tuong truong-tim-kiem">
        <i className="icon icon-kinh-lup-vien truong-bieu-tuong" />
        <input
          type="text"
          autoComplete="off"
          {...register('tuKhoa', {
            onChange: (e) => {
              const tuKhoa = R.pathOr('', ['currentTarget', 'value'])(e)

              nhomTimeout.traCuuNhaDat && clearTimeout(nhomTimeout.traCuuNhaDat)
              nhomTimeout.traCuuNhaDat = setTimeout(() => {
                xuLyTimKiemTraCuuChung(prefixLoc)({
                  loaiTimKiem,
                  tuKhoa,
                })
              }, 500)
            },
          })}
          placeholder="Nhập từ khoá tìm kiếm..."
          onFocus={() => {
            xuLyAnHienGoiYTimKiemTraCuu(true)
          }}
        />
        <i
          className="icon icon-nut-x-hinh-tron truong-tac-vu"
          onClick={() => {
            reset({
              ...loc,
              tuKhoa: '',
            })
            // xuLyAnHienGoiYTimKiemTraCuu(false)
            xuLyTimKiemTraCuuChung(prefixLoc)({ tuKhoa: '' })
          }}
        />

        {hienGoiY && <DSGoiYTimKiemTraCuuChung {...thuocTinhDSGoiYTimKiemTraCuuChung} />}
      </div>
    </form>
  )
}

TimKiemDiaChiChung.propTypes = propTypes
TimKiemDiaChiChung.defaultProps = defaultProps

export default TimKiemTraCuuChung
