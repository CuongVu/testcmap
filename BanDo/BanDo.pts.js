import PropTypes from 'prop-types'

export const propTypes = {
  className: PropTypes.string,
  xuLyThayDoi: PropTypes.func,
  xuLyNhapChuot: PropTypes.func,
}

export const defaultProps = {
  className: '',
  xuLyThayDoi: () => {},
  xuLyNhapChuot: () => {},
}
