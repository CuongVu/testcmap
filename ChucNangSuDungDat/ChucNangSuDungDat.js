import React, { useState } from 'react'
import * as R from 'ramda'

import { propTypes, defaultProps } from './ChucNangSuDungDat.pts'

const ChucNangSuDungDat = (props) => {
  const { chucNangSuDungDat } = props

  const duLieuChucNangSDD = R.pathOr({}, ['duLieu', 'colors'])(chucNangSuDungDat)

  const duLieuTuyChinh = R.pipe(
    R.filter((phanTu) => phanTu.length !== 0),
    R.toPairs,
    R.map(([label, item]) => ({ item, label: item[0]?.category }))
  )(duLieuChucNangSDD)
  const mapIndex = R.addIndex(R.map)

  const [mo, capNhatMo] = useState(true)
  const [moCon, capNhatMoCon] = useState({})
  const xuLyTacVuDongMo = () => {
    capNhatMo((prevState) => {
      return !prevState
    })
  }

  const xuLyMoConNoiBo = (name) => {
    capNhatMoCon((prevState) => {
      return {
        ...prevState,
        [name]: !moCon[name],
      }
    })
  }

  const hienThiDS = () => {
    return mapIndex((item, index) => {
      return (
        <div className="c-chuc-nang-su-dung-dat-the" key={index}>
          <div className="c-chuc-nang-su-dung-dat-cha" onClick={() => xuLyMoConNoiBo(item.label)}>
            <div className="c-chuc-nang-su-dung-dat-cha-tieu-de">{item?.label}</div>
            <div className="c-chuc-nang-su-dung-dat-cha-tac-vu">
              <i className={`icon icon-mui-ten-phai ${moCon[item.label] ? 'dang-chon' : ''}`} />
            </div>
          </div>
          <div className={`c-chuc-nang-su-dung-dat-con ${moCon[item.label] ? 'dang-chon' : ''}`}>
            <div className="c-nhom-du-lieu-bang">
              {mapIndex((itemCon, indexCon) => {
                return (
                  <div className="c-du-lieu-bang" key={indexCon + index}>
                    <label>
                      <span className="hop-mau" style={{ backgroundColor: itemCon.color }} />
                      {itemCon.name}
                    </label>
                  </div>
                )
              })(item.item)}
            </div>
          </div>
        </div>
      )
    })(duLieuTuyChinh)
  }

  return (
    <div className={`c-chuc-nang-su-dung-dat ${mo ? 'dang-chon' : ''}`}>
      <h2 className="c-chuc-nang-su-dung-dat-tieu-de">
        <i className="icon icon-kim-tu-thap-hinh" />
        &nbsp;Chức năng sử dụng đất
      </h2>

      <div className="c-chuc-nang-su-dung-dat-bo-cuc">{hienThiDS()}</div>
      <div className="c-chuc-nang-su-dung-dat-mui-ten-tac-vu" onClick={() => xuLyTacVuDongMo()}>
        <i className={`icon icon-mui-ten-${mo ? 'trai' : 'phai'}`} />
      </div>
    </div>
  )
}

ChucNangSuDungDat.propTypes = propTypes
ChucNangSuDungDat.defaultProps = defaultProps

export default ChucNangSuDungDat
