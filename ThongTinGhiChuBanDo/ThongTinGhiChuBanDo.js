/* eslint-disable react/display-name */
/* eslint-disable camelcase */
import React, { useState } from 'react'
import * as R from 'ramda'
import { Tab, GhiChuChucNangBanDo, GhiChuMDSDDBanDo, GhiChuThongTinDuLieuBanDo } from '@components'

import { propTypes, defaultProps } from './ThongTinGhiChuBanDo.pts'

const ThongTinGhiChuBanDo = (props) => {
  const { thongTinGhiChuCMap } = props
  const [moChiTiet, tatMoChiTiet] = useState(false)
  const [tabDangChon, quanLyTab] = useState('chucNangBanDoTab')
  const thuocTinhThongTinGhiChuBanDoTab = {
    loaiTab: 1,
    tabDangChon: {
      ma: tabDangChon,
    },
    xuLyTab: (tab = {}) => {
      quanLyTab(tab.ma)
    },
    dsTab: [
      {
        ten: (
          <>
            <i className="icon icon-ban-do-hinh" />
            &nbsp;Chức năng bản đồ
          </>
        ),
        ma: 'chucNangBanDoTab',
      },
      {
        ten: (
          <>
            <i className="icon icon-may-chu-vien" />
            &nbsp;Thông tin dữ liệu bản đồ
          </>
        ),
        ma: 'thongTinDuLieuBanDoTab',
      },
    ],
  }
  const thuocTinhGhiChuMDSDDBanDo = {
    duLieu: R.pathOr(null, ['duLieu', 'colors'])(thongTinGhiChuCMap),
  }
  const thongTinGhiChuThongTinDuLieuBanDo = {
    duLieu: R.pathOr(null, ['duLieu', 'citics'])(thongTinGhiChuCMap),
  }

  return (
    <div className="c-thong-tin-ghi-chu-ban-do">
      <button
        className={`nut nut-vuong nut-trang nut-do-bong ${moChiTiet ? 'c-thong-tin-ghi-chu-ban-do-nut-mo' : ''}`}
        onClick={() => {
          tatMoChiTiet(!moChiTiet)
        }}
      >
        <i className="icon icon-thong-tin-vien-tron" />
      </button>

      {moChiTiet && (
        <div className="c-thong-tin-ghi-chu-ban-do-chi-tiet">
          <div className="c-thong-tin-ghi-chu-ban-do-thanh-tieu-de">
            <i className="icon icon-thong-tin-vien-tron" />
            &nbsp;<strong>Thông tin ghi chú bản đồ</strong>
            <i
              className="icon icon-nut-x-hinh c-thong-tin-ghi-chu-ban-do-nut-dong"
              onClick={() => {
                tatMoChiTiet(false)
              }}
            />
          </div>
          <Tab {...thuocTinhThongTinGhiChuBanDoTab} />
          {tabDangChon === 'chucNangBanDoTab' ? (
            <GhiChuChucNangBanDo />
          ) : tabDangChon === 'thongTinDuLieuBanDoTab' ? (
            <GhiChuThongTinDuLieuBanDo {...thongTinGhiChuThongTinDuLieuBanDo} />
          ) : (
            <GhiChuMDSDDBanDo {...thuocTinhGhiChuMDSDDBanDo} />
          )}
        </div>
      )}
    </div>
  )
}

ThongTinGhiChuBanDo.propTypes = propTypes

ThongTinGhiChuBanDo.defaultProps = defaultProps

export default ThongTinGhiChuBanDo
